# About Me #

Hello! I am Karthik Padmanabhan from Chennai, INDIA. My preferred email is karthik.padmanabhan@smu.ca and my A# is A00424004.

I’ve done variety things, but through it all, I’ve landed primarily on Programming, Gaming and Software Engineering. That’s the common thread between all the things I’ve been doing. 

I started my schooling with a passion for Computer Science. I also always thought how games were made. This created an interest in me for games. I have worked on various Games made with Unity3D targeting Mobiles, PCs/Tablets and VR Headsets. 

Also, this evolved my horizon of knowledge as I started using various new technologies to solve demanding problems. One thing I really enjoy is the amount of help and content that is available to one who wants to learn a new technology. It’s one’s own interest and passion that must drive him/her to learn and explore. 

I want to now get out and learn something new that will make me feel better than what I know already. Playing with shaders, multiplayer game concepts, machine learning, neural networks, artificial intelligence and creating developer friendly tools have been my current target interest. Hope I can satisfy my thirst and prosper.

### Any topics you might want to see covered? ###

* None as of now.

### Familiarity with technologies talked about? ###

* .NET - worked on from projects and windows applications
* C# - primary language used for development
* java - learnt core java, jdbc, servlets, etc.
* ASP.NET - used and have good knowledge to create projects
* git - have used github, bitbucket and other version control tools to manage game projects

### Anything else relevant to the course you want to share? ###

* None as of now.