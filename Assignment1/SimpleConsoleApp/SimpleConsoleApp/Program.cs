﻿//  File ContactGenerator
//  Sample code was taken from:
//  http://www.csharpprogramming.tips/2013/06/RandomDoxGenerator.html
//  Other useful methods are there.
//
// Requirements:
// Exapand on the below example to create a CSV file (https://en.wikipedia.org/wiki/Comma-separated_values)
// For contacts with the following data
// First Name
// Last Name
// Street Number
// City
// Province
// Country  == Canada ( Simply insert "canada")
// Postal Code  ( they can be read form a file for this example if you choose, or generate if you wish)
// Phone Number ( they can be read form a file for this example if you choose, or generate if you wish)
// email Address ( Append firstname.lastname against a series for domain names read for a file
//
// Please always try to write clean and readable code
// Here is a good reference doc http://ricardogeek.com/docs/clean_code.html  
// Submit to Bitbucket under Assignment1

// 

using System;
using System.IO;
using System.Linq;

// Describes what is a namespace 
// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/namespaces/

namespace SimpleConsoleApp
{
    class ContactGenerator
    {
        // COMMA, NEWLINE and COUNTRY are predefined variables.
        string COMMA = ",";
        string NEWLINE = "\n";
        string COUNTRY = "canada";

        // chars and numbers store a set of alpha numeric characters and numbers respc.
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        const string numbers = "0123456789";

        // path to the input and output files.
        string firstNames = @".\firstNames.txt", lastNames = @".\lastNames.txt";
        string cityNames = @".\cityNames.txt", provinceNames = @".\provinceNames.txt", domainNames = @".\domainNames.txt";
        string outputFileName = @".\customers.csv";

        // instance of random number generator
        Random rand = new Random();

        static void Main(string[] args)
        {
            // instance of ContactGenerator
            ContactGenerator dg = new ContactGenerator();
        }

        public ContactGenerator()
        {
            if (File.Exists(outputFileName))
            {
                Console.Write(" File " + outputFileName + " exists, appending");
            }
            StreamWriter fileStream = new StreamWriter(outputFileName, true);

            // Write Header
            fileStream.Write(
                "First Name" + COMMA + "Last Name" + COMMA +
                "Street Number" + COMMA + "City" + COMMA +
                "Province" + COMMA + "Country" + COMMA +
                "Postal Code" + COMMA + "Phone Number" + COMMA + "Email Address" + 
                NEWLINE);

            for (int i = 0; i < 20; i++)
            {
                fileStream.Write(GenerateProfile() + NEWLINE);
            }
            fileStream.Close();
        }

        /// <summary>
        /// THe method returns a random profile data in comma seperated format.
        /// </summary>
        /// <returns>string = profile data</returns>
        public string GenerateProfile()
        {
            // variables used to store values
            string profileData = string.Empty;
            string firstName, lastName;
            // stores a randomly generated street number
            string streetNumber = rand.Next(1111,9999).ToString();

            // fetches a random firstname.
            firstName = ReturnRandomLine(firstNames);
            // fetces a random last name.
            lastName = ReturnRandomLine(lastNames);
            // creates profile data by using the random firstname and lastname; while randomly generating the other datas.
            profileData += firstName + COMMA + lastName + COMMA + 
                streetNumber + COMMA + ReturnRandomLine(cityNames) + COMMA +
                ReturnRandomLine(provinceNames) + COMMA + COUNTRY + COMMA +
                new string(Enumerable.Repeat(chars, 3).Select(s => s[rand.Next(s.Length)]).ToArray()) + " " +
                new string(Enumerable.Repeat(chars, 3).Select(s => s[rand.Next(s.Length)]).ToArray()) + COMMA +
                new string(Enumerable.Repeat(numbers, 3).Select(s => s[rand.Next(s.Length)]).ToArray()) + "-" +
                new string(Enumerable.Repeat(numbers, 3).Select(s => s[rand.Next(s.Length)]).ToArray()) + "-" +
                new string(Enumerable.Repeat(numbers, 4).Select(s => s[rand.Next(s.Length)]).ToArray()) + COMMA +
                firstName + "." + lastName + "@" + ReturnRandomLine(domainNames);

            return profileData;
        }

        // Gets a line from a file
        public string ReturnRandomLine(string FileName)
        {
            string sReturn = string.Empty;

            using (FileStream myFile = new FileStream(FileName, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader myStream = new StreamReader(myFile))
                {

                    // just cast it to int because we know it will be less than 
                    int fileLength = (int)myFile.Length;

                    // Seek file stream pointer to a rand position...
                    myStream.BaseStream.Seek(rand.Next(1, fileLength), SeekOrigin.Begin);

                    // Read the rest of that line.
                    myStream.ReadLine();

                    // Return the next, full line...
                    sReturn = myStream.ReadLine();
                }
            }

            // If our random file position was too close to the end of the file, it will return an empty string
            // I avoided a while loop in the case that the file is empty or contains only one line
            if (System.String.IsNullOrWhiteSpace(sReturn))
            {
                sReturn = ReturnRandomLine(FileName);
            }

            return sReturn;
        }
    }
}