﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing;

namespace Assignment_2
{
    /// <summary>
    /// Main Contact Form
    /// </summary>
    public partial class Contact_Form : Form
    {
	List<Contact> contacts = new List<Contact>();
	List<Contact> contactsTable = new List<Contact>();
	int currentIndex, previousIndex, nextIndex;

	/// <summary>
	/// Constructor
	/// </summary>
	public Contact_Form()
	{
	    InitializeComponent();
	}

	/// <summary>
	/// Browse Button Function
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void btnBrowse_Click(object sender, System.EventArgs e)
	{
	    openFileDialog.ShowDialog();
	}

	/// <summary>
	/// OpenFileDialog Function
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void openFileDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
	{
	    tbFilePath.Text = openFileDialog.FileName;
	}

	/// <summary>
	/// Import Button Function
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void btnImport_Click(object sender, EventArgs e)
	{
	    string fileData = string.Empty;
	    if (tbFilePath.Text == string.Empty || tbFilePath.Text == null)
		return;

	    using (FileStream fileStream = new FileStream(tbFilePath.Text, FileMode.Open, FileAccess.Read))
	    {
		using (StreamReader streamReader = new StreamReader(fileStream))
		{
		    var datas = ExternalParser.CsvParser.ParseHeadAndTail(streamReader, ',', '"');
		    foreach (var data in datas.Item2)
		    {
			Contact c = new Contact();
			c.firstName = data[0];
			c.lastName = data[1];
			c.streetNumber = int.Parse(data[2]);
			c.city = data[3];
			c.province = data[4];
			c.country = data[5];
			c.postalCode = data[6];
			c.phoneNumber = data[7].Replace("-","");
			c.enailAddress = data[8];
			contacts.Add(c);
		    }
		}
	    }

	    SqlConnection sqlConnection = null;
	    SqlCommand sqlCommand;
	    SqlDataReader sqlDataReader;
	    string COMMA = "\',\'";
	    try
	    {
		sqlConnection = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("Assignment 2.exe", "") + "Database\\Contacts_Database.mdf;Integrated Security=True");
		sqlConnection.Open();

		foreach (Contact c in contacts)
		{
		    string query = "INSERT INTO Contacts (FirstName,LastName,StreetNumber,Address,City,Province,Country,PostalCode,PhoneNumber,EmailAddress) VALUES (\'" + c.firstName + COMMA + c.lastName + "\'," + c.streetNumber + ",\'" + RandomLine.ReturnRandomLine(@".\Address.txt") + COMMA + c.city + COMMA + c.province + COMMA + c.country + COMMA + c.postalCode + COMMA + c.phoneNumber + COMMA + c.enailAddress + "\');";
		    sqlCommand = new SqlCommand(query, sqlConnection);
		    sqlDataReader = sqlCommand.ExecuteReader();
		    sqlDataReader.Close();
		}
		MessageBox.Show("CSV FIle Imported.");
		DisplayData();
	    }
	    catch (Exception err)
	    {
		MessageBox.Show(err.Message);
	    }
	    finally
	    {
		sqlConnection.Close();
	    }
	}

	/// <summary>
	/// DIsplay Function
	/// </summary>
	private void DisplayData()
	{
	    SqlConnection sqlConnection = null;
	    SqlCommand sqlCommand;
	    SqlDataReader sqlDataReader;

	    try
	    {
		sqlConnection = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("Assignment 2.exe", "") + "Database\\Contacts_Database.mdf;Integrated Security=True");
		sqlConnection.Open();

		foreach (Contact c in contacts)
		{
		    string query = "SELECT * FROM Contacts;";
		    sqlCommand = new SqlCommand(query, sqlConnection);
		    sqlDataReader = sqlCommand.ExecuteReader();
		    while (sqlDataReader.Read())
		    {
			Contact contact = new Contact();
			contact.primaryKey = sqlDataReader["Id"].ToString();
			contact.firstName = sqlDataReader["FirstName"].ToString();
			contact.lastName = sqlDataReader["LastName"].ToString();
			contact.streetNumber = int.Parse(sqlDataReader["StreetNumber"].ToString());
			contact.address = sqlDataReader["Address"].ToString();
			contact.city = sqlDataReader["City"].ToString();
			contact.province = sqlDataReader["Province"].ToString();
			contact.country = sqlDataReader["Country"].ToString();
			contact.postalCode = sqlDataReader["PostalCode"].ToString();
			contact.phoneNumber = sqlDataReader["PhoneNumber"].ToString();
			contact.enailAddress = sqlDataReader["EmailAddress"].ToString();
			contactsTable.Add(contact);
		    }
		    sqlDataReader.Close();
		}
	    }
	    catch (Exception err)
	    {
		MessageBox.Show(err.Message);
	    }
	    finally
	    {
		sqlConnection.Close();
	    }

	    btnPrevious.Enabled = false;
	    previousIndex = -1;
	    currentIndex = 0;
	    nextIndex = currentIndex + 1;
	    Display(currentIndex);
	}

	private void btnPrevious_Click(object sender, EventArgs e)
	{
	    if (previousIndex != -1)
	    {
		btnNext.Enabled = true;
		previousIndex--;
		currentIndex--;
		nextIndex--;
		if (previousIndex == -1)
		    btnPrevious.Enabled = false;
		else
		    btnPrevious.Enabled = true;
		Display(currentIndex);
	    }
	}

	private void btnNext_Click(object sender, EventArgs e)
	{
	    if (currentIndex != contacts.Count)
	    {
		btnPrevious.Enabled = true;
		previousIndex++;
		currentIndex++;
		nextIndex++;
		if (nextIndex == contacts.Count)
		    btnNext.Enabled = false;
		else
		    btnNext.Enabled = true;
		Display(currentIndex);
	    }
	}

	private void Display(int currentIndex)
	{
	    tbPKey.Text = contactsTable[currentIndex].primaryKey;
	    tbFName.Text = contactsTable[currentIndex].firstName;
	    tbLName.Text = contactsTable[currentIndex].lastName;
	    tbStreetNo.Text = contactsTable[currentIndex].streetNumber.ToString();
	    tbAddress.Text = contactsTable[currentIndex].address;
	    tbCity.Text = contactsTable[currentIndex].city;
	    tbProvince.Text = contactsTable[currentIndex].province;
	    tbCountry.Text = contactsTable[currentIndex].country;
	    tbPostalCode.Text = contactsTable[currentIndex].postalCode;
	    tbPhoneNo.Text = contactsTable[currentIndex].phoneNumber;
	    tbEmail.Text = contactsTable[currentIndex].enailAddress;

	    if (CheckValidation(tbFName))
		lblStatus.Text = "FirstName is more than 50 Chars.";
	    else if (CheckValidation(tbLName))
		lblStatus.Text = "LastName is more than 50 Chars.";
	    else if (CheckValidation(tbAddress))
		lblStatus.Text = "Address is more than 50 Chars.";
	    else if (CheckValidation(tbCity))
		lblStatus.Text = "City is more than 50 Chars.";
	    else if (CheckValidation(tbProvince))
		lblStatus.Text = "Province is more than 50 Chars.";
	    else
		lblStatus.Text = "Status: Idle.";
	}

	private bool CheckValidation(TextBox tb)
	{
	    if (tb.Text.Length > 50)
	    {
		tb.BackColor = Color.Red;
		return true;
	    }
	    else
	    {
		tb.BackColor = Color.White;
		return false;
	    }
	}
    }
}
