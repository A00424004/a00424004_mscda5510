﻿namespace Assignment_2
{
    /// <summary>
    /// Contact Class store the Contact Data for each contact.
    /// </summary>
    public class Contact
    {
	public string primaryKey;
	public string firstName;
	public string lastName;
	public int streetNumber;
	public string address;
	public string city;
	public string province;
	public string country;
	public string postalCode;
	public string phoneNumber;
	public string enailAddress;
    }
}
