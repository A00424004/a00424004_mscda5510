﻿namespace Assignment_2
{
    partial class Contact_Form
    {
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	/// <summary>
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing)
	{
	    if (disposing && (components != null))
	    {
		components.Dispose();
	    }
	    base.Dispose(disposing);
	}

	#region Windows Form Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent()
	{
	    this.tbPKey = new System.Windows.Forms.TextBox();
	    this.tbFName = new System.Windows.Forms.TextBox();
	    this.tbLName = new System.Windows.Forms.TextBox();
	    this.tbStreetNo = new System.Windows.Forms.TextBox();
	    this.tbCity = new System.Windows.Forms.TextBox();
	    this.tbProvince = new System.Windows.Forms.TextBox();
	    this.tbCountry = new System.Windows.Forms.TextBox();
	    this.tbPostalCode = new System.Windows.Forms.TextBox();
	    this.tbPhoneNo = new System.Windows.Forms.TextBox();
	    this.tbEmail = new System.Windows.Forms.TextBox();
	    this.label1 = new System.Windows.Forms.Label();
	    this.label2 = new System.Windows.Forms.Label();
	    this.label3 = new System.Windows.Forms.Label();
	    this.label4 = new System.Windows.Forms.Label();
	    this.label5 = new System.Windows.Forms.Label();
	    this.label6 = new System.Windows.Forms.Label();
	    this.label7 = new System.Windows.Forms.Label();
	    this.label8 = new System.Windows.Forms.Label();
	    this.label9 = new System.Windows.Forms.Label();
	    this.label10 = new System.Windows.Forms.Label();
	    this.label11 = new System.Windows.Forms.Label();
	    this.panel1 = new System.Windows.Forms.Panel();
	    this.btnPrevious = new System.Windows.Forms.Button();
	    this.btnNext = new System.Windows.Forms.Button();
	    this.panel2 = new System.Windows.Forms.Panel();
	    this.btnImport = new System.Windows.Forms.Button();
	    this.btnBrowse = new System.Windows.Forms.Button();
	    this.tbFilePath = new System.Windows.Forms.TextBox();
	    this.label12 = new System.Windows.Forms.Label();
	    this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
	    this.statusStrip = new System.Windows.Forms.StatusStrip();
	    this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
	    this.label13 = new System.Windows.Forms.Label();
	    this.tbAddress = new System.Windows.Forms.TextBox();
	    this.panel1.SuspendLayout();
	    this.panel2.SuspendLayout();
	    this.statusStrip.SuspendLayout();
	    this.SuspendLayout();
	    // 
	    // tbPKey
	    // 
	    this.tbPKey.Location = new System.Drawing.Point(149, 51);
	    this.tbPKey.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
	    this.tbPKey.Name = "tbPKey";
	    this.tbPKey.ReadOnly = true;
	    this.tbPKey.Size = new System.Drawing.Size(92, 25);
	    this.tbPKey.TabIndex = 0;
	    // 
	    // tbFName
	    // 
	    this.tbFName.Location = new System.Drawing.Point(149, 84);
	    this.tbFName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
	    this.tbFName.Name = "tbFName";
	    this.tbFName.Size = new System.Drawing.Size(194, 25);
	    this.tbFName.TabIndex = 1;
	    // 
	    // tbLName
	    // 
	    this.tbLName.Location = new System.Drawing.Point(149, 117);
	    this.tbLName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
	    this.tbLName.Name = "tbLName";
	    this.tbLName.Size = new System.Drawing.Size(194, 25);
	    this.tbLName.TabIndex = 2;
	    // 
	    // tbStreetNo
	    // 
	    this.tbStreetNo.Location = new System.Drawing.Point(149, 150);
	    this.tbStreetNo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
	    this.tbStreetNo.Name = "tbStreetNo";
	    this.tbStreetNo.Size = new System.Drawing.Size(92, 25);
	    this.tbStreetNo.TabIndex = 3;
	    // 
	    // tbCity
	    // 
	    this.tbCity.Location = new System.Drawing.Point(149, 214);
	    this.tbCity.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
	    this.tbCity.Name = "tbCity";
	    this.tbCity.Size = new System.Drawing.Size(116, 25);
	    this.tbCity.TabIndex = 4;
	    // 
	    // tbProvince
	    // 
	    this.tbProvince.Location = new System.Drawing.Point(149, 247);
	    this.tbProvince.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
	    this.tbProvince.Name = "tbProvince";
	    this.tbProvince.Size = new System.Drawing.Size(116, 25);
	    this.tbProvince.TabIndex = 5;
	    // 
	    // tbCountry
	    // 
	    this.tbCountry.Location = new System.Drawing.Point(149, 280);
	    this.tbCountry.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
	    this.tbCountry.Name = "tbCountry";
	    this.tbCountry.Size = new System.Drawing.Size(116, 25);
	    this.tbCountry.TabIndex = 6;
	    // 
	    // tbPostalCode
	    // 
	    this.tbPostalCode.Location = new System.Drawing.Point(149, 313);
	    this.tbPostalCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
	    this.tbPostalCode.Name = "tbPostalCode";
	    this.tbPostalCode.Size = new System.Drawing.Size(92, 25);
	    this.tbPostalCode.TabIndex = 7;
	    // 
	    // tbPhoneNo
	    // 
	    this.tbPhoneNo.Location = new System.Drawing.Point(149, 346);
	    this.tbPhoneNo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
	    this.tbPhoneNo.Name = "tbPhoneNo";
	    this.tbPhoneNo.Size = new System.Drawing.Size(116, 25);
	    this.tbPhoneNo.TabIndex = 8;
	    // 
	    // tbEmail
	    // 
	    this.tbEmail.Location = new System.Drawing.Point(149, 379);
	    this.tbEmail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
	    this.tbEmail.Name = "tbEmail";
	    this.tbEmail.Size = new System.Drawing.Size(309, 25);
	    this.tbEmail.TabIndex = 9;
	    // 
	    // label1
	    // 
	    this.label1.AutoSize = true;
	    this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
	    this.label1.Location = new System.Drawing.Point(129, 9);
	    this.label1.Name = "label1";
	    this.label1.Size = new System.Drawing.Size(257, 30);
	    this.label1.TabIndex = 10;
	    this.label1.Text = "CONTACT FORM EDITOR";
	    // 
	    // label2
	    // 
	    this.label2.AutoSize = true;
	    this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
	    this.label2.Location = new System.Drawing.Point(37, 54);
	    this.label2.Name = "label2";
	    this.label2.Size = new System.Drawing.Size(107, 17);
	    this.label2.TabIndex = 11;
	    this.label2.Text = "Primary Key: (*)";
	    // 
	    // label3
	    // 
	    this.label3.AutoSize = true;
	    this.label3.Location = new System.Drawing.Point(69, 87);
	    this.label3.Name = "label3";
	    this.label3.Size = new System.Drawing.Size(74, 17);
	    this.label3.TabIndex = 12;
	    this.label3.Text = "First Name:";
	    // 
	    // label4
	    // 
	    this.label4.AutoSize = true;
	    this.label4.Location = new System.Drawing.Point(70, 120);
	    this.label4.Name = "label4";
	    this.label4.Size = new System.Drawing.Size(73, 17);
	    this.label4.TabIndex = 13;
	    this.label4.Text = "Last Name:";
	    // 
	    // label5
	    // 
	    this.label5.AutoSize = true;
	    this.label5.Location = new System.Drawing.Point(76, 153);
	    this.label5.Name = "label5";
	    this.label5.Size = new System.Drawing.Size(67, 17);
	    this.label5.TabIndex = 14;
	    this.label5.Text = "Street No:";
	    // 
	    // label6
	    // 
	    this.label6.AutoSize = true;
	    this.label6.Location = new System.Drawing.Point(111, 217);
	    this.label6.Name = "label6";
	    this.label6.Size = new System.Drawing.Size(32, 17);
	    this.label6.TabIndex = 15;
	    this.label6.Text = "City:";
	    // 
	    // label7
	    // 
	    this.label7.AutoSize = true;
	    this.label7.Location = new System.Drawing.Point(83, 250);
	    this.label7.Name = "label7";
	    this.label7.Size = new System.Drawing.Size(60, 17);
	    this.label7.TabIndex = 16;
	    this.label7.Text = "Province:";
	    // 
	    // label8
	    // 
	    this.label8.AutoSize = true;
	    this.label8.Location = new System.Drawing.Point(87, 283);
	    this.label8.Name = "label8";
	    this.label8.Size = new System.Drawing.Size(56, 17);
	    this.label8.TabIndex = 17;
	    this.label8.Text = "Country:";
	    // 
	    // label9
	    // 
	    this.label9.AutoSize = true;
	    this.label9.Location = new System.Drawing.Point(63, 316);
	    this.label9.Name = "label9";
	    this.label9.Size = new System.Drawing.Size(81, 17);
	    this.label9.TabIndex = 18;
	    this.label9.Text = "Postal Code:";
	    // 
	    // label10
	    // 
	    this.label10.AutoSize = true;
	    this.label10.Location = new System.Drawing.Point(74, 349);
	    this.label10.Name = "label10";
	    this.label10.Size = new System.Drawing.Size(69, 17);
	    this.label10.TabIndex = 19;
	    this.label10.Text = "Phone No:";
	    // 
	    // label11
	    // 
	    this.label11.AutoSize = true;
	    this.label11.Location = new System.Drawing.Point(85, 382);
	    this.label11.Name = "label11";
	    this.label11.Size = new System.Drawing.Size(58, 17);
	    this.label11.TabIndex = 20;
	    this.label11.Text = "Email ID:";
	    // 
	    // panel1
	    // 
	    this.panel1.Controls.Add(this.btnPrevious);
	    this.panel1.Controls.Add(this.btnNext);
	    this.panel1.Location = new System.Drawing.Point(0, 414);
	    this.panel1.Name = "panel1";
	    this.panel1.Size = new System.Drawing.Size(511, 52);
	    this.panel1.TabIndex = 21;
	    // 
	    // btnPrevious
	    // 
	    this.btnPrevious.Location = new System.Drawing.Point(12, 8);
	    this.btnPrevious.Name = "btnPrevious";
	    this.btnPrevious.Size = new System.Drawing.Size(109, 35);
	    this.btnPrevious.TabIndex = 10;
	    this.btnPrevious.Text = "<< Previous";
	    this.btnPrevious.UseVisualStyleBackColor = true;
	    this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
	    // 
	    // btnNext
	    // 
	    this.btnNext.Location = new System.Drawing.Point(393, 8);
	    this.btnNext.Name = "btnNext";
	    this.btnNext.Size = new System.Drawing.Size(96, 35);
	    this.btnNext.TabIndex = 11;
	    this.btnNext.Text = "Next >>";
	    this.btnNext.UseVisualStyleBackColor = true;
	    this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
	    // 
	    // panel2
	    // 
	    this.panel2.Controls.Add(this.btnImport);
	    this.panel2.Controls.Add(this.btnBrowse);
	    this.panel2.Controls.Add(this.tbFilePath);
	    this.panel2.Controls.Add(this.label12);
	    this.panel2.Location = new System.Drawing.Point(0, 467);
	    this.panel2.Name = "panel2";
	    this.panel2.Size = new System.Drawing.Size(511, 113);
	    this.panel2.TabIndex = 22;
	    // 
	    // btnImport
	    // 
	    this.btnImport.Location = new System.Drawing.Point(301, 64);
	    this.btnImport.Name = "btnImport";
	    this.btnImport.Size = new System.Drawing.Size(96, 35);
	    this.btnImport.TabIndex = 15;
	    this.btnImport.Text = "Import";
	    this.btnImport.UseVisualStyleBackColor = true;
	    this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
	    // 
	    // btnBrowse
	    // 
	    this.btnBrowse.Location = new System.Drawing.Point(403, 64);
	    this.btnBrowse.Name = "btnBrowse";
	    this.btnBrowse.Size = new System.Drawing.Size(96, 35);
	    this.btnBrowse.TabIndex = 14;
	    this.btnBrowse.Text = "Browse";
	    this.btnBrowse.UseVisualStyleBackColor = true;
	    this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
	    // 
	    // tbFilePath
	    // 
	    this.tbFilePath.Location = new System.Drawing.Point(12, 33);
	    this.tbFilePath.Name = "tbFilePath";
	    this.tbFilePath.ReadOnly = true;
	    this.tbFilePath.Size = new System.Drawing.Size(487, 25);
	    this.tbFilePath.TabIndex = 13;
	    // 
	    // label12
	    // 
	    this.label12.AutoSize = true;
	    this.label12.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
	    this.label12.Location = new System.Drawing.Point(12, 13);
	    this.label12.Name = "label12";
	    this.label12.Size = new System.Drawing.Size(117, 17);
	    this.label12.TabIndex = 23;
	    this.label12.Text = "Import File (.csv):";
	    // 
	    // openFileDialog
	    // 
	    this.openFileDialog.FileName = "openFileDialog";
	    this.openFileDialog.Filter = "*.csv|*.CSV";
	    this.openFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog_FileOk);
	    // 
	    // statusStrip
	    // 
	    this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
	    this.statusStrip.Location = new System.Drawing.Point(0, 582);
	    this.statusStrip.Name = "statusStrip";
	    this.statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
	    this.statusStrip.Size = new System.Drawing.Size(511, 22);
	    this.statusStrip.SizingGrip = false;
	    this.statusStrip.TabIndex = 23;
	    this.statusStrip.Text = "statusStrip";
	    // 
	    // lblStatus
	    // 
	    this.lblStatus.Name = "lblStatus";
	    this.lblStatus.Size = new System.Drawing.Size(64, 17);
	    this.lblStatus.Text = "Status: Idle";
	    // 
	    // label13
	    // 
	    this.label13.AutoSize = true;
	    this.label13.Location = new System.Drawing.Point(85, 184);
	    this.label13.Name = "label13";
	    this.label13.Size = new System.Drawing.Size(59, 17);
	    this.label13.TabIndex = 25;
	    this.label13.Text = "Address:";
	    // 
	    // tbAddress
	    // 
	    this.tbAddress.Location = new System.Drawing.Point(149, 181);
	    this.tbAddress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
	    this.tbAddress.Name = "tbAddress";
	    this.tbAddress.Size = new System.Drawing.Size(116, 25);
	    this.tbAddress.TabIndex = 24;
	    // 
	    // Contact_Form
	    // 
	    this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
	    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
	    this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
	    this.ClientSize = new System.Drawing.Size(511, 604);
	    this.Controls.Add(this.label13);
	    this.Controls.Add(this.tbAddress);
	    this.Controls.Add(this.statusStrip);
	    this.Controls.Add(this.panel2);
	    this.Controls.Add(this.panel1);
	    this.Controls.Add(this.label11);
	    this.Controls.Add(this.label10);
	    this.Controls.Add(this.label9);
	    this.Controls.Add(this.label8);
	    this.Controls.Add(this.label7);
	    this.Controls.Add(this.label6);
	    this.Controls.Add(this.label5);
	    this.Controls.Add(this.label4);
	    this.Controls.Add(this.label3);
	    this.Controls.Add(this.label2);
	    this.Controls.Add(this.label1);
	    this.Controls.Add(this.tbEmail);
	    this.Controls.Add(this.tbPhoneNo);
	    this.Controls.Add(this.tbPostalCode);
	    this.Controls.Add(this.tbCountry);
	    this.Controls.Add(this.tbProvince);
	    this.Controls.Add(this.tbCity);
	    this.Controls.Add(this.tbStreetNo);
	    this.Controls.Add(this.tbLName);
	    this.Controls.Add(this.tbFName);
	    this.Controls.Add(this.tbPKey);
	    this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
	    this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
	    this.MaximizeBox = false;
	    this.MinimizeBox = false;
	    this.Name = "Contact_Form";
	    this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
	    this.Text = "Contact_Form";
	    this.panel1.ResumeLayout(false);
	    this.panel2.ResumeLayout(false);
	    this.panel2.PerformLayout();
	    this.statusStrip.ResumeLayout(false);
	    this.statusStrip.PerformLayout();
	    this.ResumeLayout(false);
	    this.PerformLayout();

	}

	#endregion

	private System.Windows.Forms.TextBox tbPKey;
	private System.Windows.Forms.TextBox tbFName;
	private System.Windows.Forms.TextBox tbLName;
	private System.Windows.Forms.TextBox tbStreetNo;
	private System.Windows.Forms.TextBox tbCity;
	private System.Windows.Forms.TextBox tbProvince;
	private System.Windows.Forms.TextBox tbCountry;
	private System.Windows.Forms.TextBox tbPostalCode;
	private System.Windows.Forms.TextBox tbPhoneNo;
	private System.Windows.Forms.TextBox tbEmail;
	private System.Windows.Forms.Label label1;
	private System.Windows.Forms.Label label2;
	private System.Windows.Forms.Label label3;
	private System.Windows.Forms.Label label4;
	private System.Windows.Forms.Label label5;
	private System.Windows.Forms.Label label6;
	private System.Windows.Forms.Label label7;
	private System.Windows.Forms.Label label8;
	private System.Windows.Forms.Label label9;
	private System.Windows.Forms.Label label10;
	private System.Windows.Forms.Label label11;
	private System.Windows.Forms.Panel panel1;
	private System.Windows.Forms.Button btnPrevious;
	private System.Windows.Forms.Button btnNext;
	private System.Windows.Forms.Panel panel2;
	private System.Windows.Forms.Button btnImport;
	private System.Windows.Forms.Button btnBrowse;
	private System.Windows.Forms.TextBox tbFilePath;
	private System.Windows.Forms.Label label12;
	private System.Windows.Forms.OpenFileDialog openFileDialog;
	private System.Windows.Forms.StatusStrip statusStrip;
	private System.Windows.Forms.ToolStripStatusLabel lblStatus;
	private System.Windows.Forms.Label label13;
	private System.Windows.Forms.TextBox tbAddress;
    }
}