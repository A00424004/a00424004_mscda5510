﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2
{
    public class RandomLine
    {
	static Random rand = new Random();
	// Gets a line from a file
	public static string ReturnRandomLine(string FileName)
	{
	    string sReturn = string.Empty;

	    using (FileStream myFile = new FileStream(FileName, FileMode.Open, FileAccess.Read))
	    {
		using (StreamReader myStream = new StreamReader(myFile))
		{

		    // just cast it to int because we know it will be less than 
		    int fileLength = (int)myFile.Length;

		    // Seek file stream pointer to a rand position...
		    myStream.BaseStream.Seek(rand.Next(1, fileLength), SeekOrigin.Begin);

		    // Read the rest of that line.
		    myStream.ReadLine();

		    // Return the next, full line...
		    sReturn = myStream.ReadLine();
		}
	    }

	    // If our random file position was too close to the end of the file, it will return an empty string
	    // I avoided a while loop in the case that the file is empty or contains only one line
	    if (System.String.IsNullOrWhiteSpace(sReturn))
	    {
		sReturn = ReturnRandomLine(FileName);
	    }

	    return sReturn;
	}
    }
}
