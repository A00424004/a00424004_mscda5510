﻿CREATE TABLE [dbo].[Contacts] (
    [Id]           INT          IDENTITY (1, 1) NOT NULL,
    [FirstName]    VARCHAR (100) NULL,
    [LastName]     VARCHAR (100) NULL,
    [StreetNumber] INT          NULL,
    [Address]      VARCHAR (100) NULL,
    [City]         VARCHAR (100) NULL,
    [Province]     VARCHAR (100) NULL,
    [Country]      VARCHAR (50) NULL,
    [PostalCode]   VARCHAR (7)  NULL,
    [PhoneNumber]  VARCHAR (10) NULL,
    [EmailAddress] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

