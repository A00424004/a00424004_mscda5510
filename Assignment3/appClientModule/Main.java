import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

public class Main {
	
	public static int valid = 0, invalid = 0;
	public static int prevValid = 0, prevInvalid = 0;
	
	public static void main(String[] args) throws RuntimeException, IOException {
		// folder path
		String dirPath = "../input/";
		// search folder for files
		long startTime = System.currentTimeMillis();
		walk(dirPath);
		long endTime = System.currentTimeMillis();

		FileHandler handler = new FileHandler("default.log", true);
		Logger logger = Logger.getAnonymousLogger();
		logger.addHandler(handler);
		logger.log(Level.INFO,"Total execution time: "+(endTime-startTime));
		logger.log(Level.INFO,"Total valid rows: "+valid);
		logger.log(Level.INFO,"Total skipped rows: "+invalid);
	}
	
    public static void walk(String path) throws FileNotFoundException {
    	PrintWriter printWriter = new PrintWriter( new FileOutputStream(new File("../output/output.csv"),true));
    	
        File root = new File( path );
        File[] list = root.listFiles();

        if (list == null) return;

        for ( File f : list ) {
            if ( f.isDirectory() )
                walk( f.getAbsolutePath() );
            else
            	parse(f.getAbsoluteFile().toString(), printWriter);
        }
        
        printWriter.close();
    }
    
    public static void parse(String path, PrintWriter printWriter) {
		Reader in;
		Iterable<CSVRecord> records = null;
		
		try {
			in = new FileReader(path);
			File root = new File( path );
			//CSVFormat csvFormat = CSVFormat.EXCEL.withHeader("First Name", "Last Name","Street Number", "Street","City","Province","Postal Code","Country","Phone Number","email Address");
			records = CSVFormat.EXCEL.parse(in);
			//records = CSVParser.parse(root, StandardCharsets.US_ASCII, csvFormat);
			}
		catch ( IOException e) {
			e.printStackTrace();
			Logger.getAnonymousLogger().log(Level.INFO,"Message - "+"Total skipped rows: "+invalid);
			}
		
		for (CSVRecord record : records) {
			prevValid = valid;
			prevInvalid = invalid;
			
		    String firstName = record.get(0);
		    String lastName = record.get(1);
		    String streetNumber = record.get(2);
		    String street = record.get(3);
		    String city = record.get(4);
		    String province = record.get(5);
		    String country = record.get(6);
		    String postalCode = record.get(7);
		    String phoneNumber = record.get(8);
		    String emailAddress = record.get(9);
		    if (!firstName.isEmpty()&&!lastName.isEmpty()&&!streetNumber.isEmpty()&&!street.isEmpty()&&
		    		!city.isEmpty()&&!province.isEmpty()&&!country.isEmpty()&&!postalCode.isEmpty()&&
		    		!phoneNumber.isEmpty()&&!emailAddress.isEmpty())
		    	valid++;
		    else
		    	invalid++;
			
//			if(prevValid != valid)
//			{
			    printWriter.write("\n");
			    printWriter.write(record.toString());
//			}
		}
	}
}