namespace EventReservationSystem___Project
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CreditCard")]
    public partial class CreditCard
    {
        public int ID { get; set; }

        [Required]
        [StringLength(20)]
        public string CreditCardType { get; set; }

        [Required]
	[DataType (DataType.CreditCard)]
        [StringLength(50)]
        public string CreditCardNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string NameOnCard { get; set; }

        [Column(TypeName = "date")]
        public DateTime ExpirationDate { get
	    {
		return new DateTime(int.Parse(Year), int.Parse(Month), 0);
	    }
	    }
	public string Month { get; set; }
	public string Year { get; set; }
        [Required]
        [StringLength(10)]
        public string CVV { get; set; }

	public int ContactID = Manager.c.ID;

        public virtual Contact Contact { get; set; }
    }
}
