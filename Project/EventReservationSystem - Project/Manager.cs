﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace EventReservationSystem___Project
{
    public class Manager
    {
	public static Contact c;

	public static bool CheckDate(System.DateTime startDate, System.DateTime endDate)
	{
	    if (endDate > startDate)
		return true;
	    else
		return false;
	}
    }

    public class PostalValidate : ValidationAttribute
    {

	protected override ValidationResult IsValid(object value,
			    ValidationContext validationContext)
	{

	    string str = value.ToString();

	    if (!IsUSorCanadianZipCode(str))
	    {
		return new ValidationResult("Invalid postal code");
	    }

	    return ValidationResult.Success;
	}

	private bool IsUSorCanadianZipCode(string zipCode)
	{
	    bool isValidUsOrCanadianZip = false;
	    string pattern = @"^\d{5}-\d{4}|\d{5}|[A-Z]\d[A-Z] \d[A-Z]\d$";
	    Regex regex = new Regex(pattern);
	    return isValidUsOrCanadianZip = regex.IsMatch(zipCode);
	}

    }

    public class EndDateValidation : ValidationAttribute
    {
	protected override ValidationResult IsValid(object value,
			    ValidationContext validationContext)
	{
	    Event events = (Event)validationContext.ObjectInstance;

	    DateTime endDate = (DateTime)value;
	    DateTime startDate = (DateTime)events.StartDate;

	    int result = DateTime.Compare(startDate, endDate);

	    if (result > 0)
	    {
		return new ValidationResult("End date cannot be earlier than Start date");
	    }
	    else if (result == 0)
	    {
		return new ValidationResult("End date cannot be the same as Start date");
	    }

	    return ValidationResult.Success;
	}
    }
}