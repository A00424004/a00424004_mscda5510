namespace EventReservationSystem___Project
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Event")]
    public partial class Event
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string EventName { get; set; }

        public int PeopleCount { get; set; }

        [Column(TypeName = "date")]
	[DataType(DataType.Date)]
	public DateTime StartDate { get; set; }

	[EndDateValidation]
	[Column(TypeName = "date")]
	[DataType(DataType.Date)]
	public DateTime EndDate { get; set; }

	//public int ContactID = Manager.c.ID;

        public virtual Contact Contact { get; set; }
    }
}
