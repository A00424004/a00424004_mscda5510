namespace EventReservationSystem___Project
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ReservationModel : DbContext
    {
	public ReservationModel()
	    : base("name=ReservationModel")
	{
	}

	public virtual DbSet<Contact> Contacts { get; set; }
	public virtual DbSet<CreditCard> CreditCards { get; set; }
	public virtual DbSet<Event> Events { get; set; }

	protected override void OnModelCreating(DbModelBuilder modelBuilder)
	{
	    modelBuilder.Entity<Contact>()
		.Property(e => e.FirstName)
		.IsUnicode(false);

	    modelBuilder.Entity<Contact>()
		.Property(e => e.LastName)
		.IsUnicode(false);

	    modelBuilder.Entity<Contact>()
		.Property(e => e.Address)
		.IsFixedLength();

	    modelBuilder.Entity<Contact>()
		.Property(e => e.City)
		.IsUnicode(false);

	    modelBuilder.Entity<Contact>()
		.Property(e => e.Province)
		.IsUnicode(false);

	    modelBuilder.Entity<Contact>()
		.Property(e => e.Country)
		.IsUnicode(false);

	    modelBuilder.Entity<Contact>()
		.Property(e => e.PostalCode)
		.IsUnicode(false);

	    modelBuilder.Entity<Contact>()
		.Property(e => e.PhoneNumber)
		.IsUnicode(false);

	    modelBuilder.Entity<Contact>()
		.Property(e => e.EmailAddress)
		.IsUnicode(false);

	    modelBuilder.Entity<Contact>()
		.HasMany(e => e.CreditCards)
		.WithRequired(e => e.Contact)
		.WillCascadeOnDelete(false);

	    modelBuilder.Entity<Contact>()
		.HasMany(e => e.Events)
		.WithRequired(e => e.Contact)
		.WillCascadeOnDelete(false);

	    modelBuilder.Entity<CreditCard>()
		.Property(e => e.CreditCardType)
		.IsUnicode(false);

	    modelBuilder.Entity<CreditCard>()
		.Property(e => e.CreditCardNumber)
		.IsUnicode(false);

	    modelBuilder.Entity<CreditCard>()
		.Property(e => e.NameOnCard)
		.IsUnicode(false);

	    modelBuilder.Entity<CreditCard>()
		.Property(e => e.CVV)
		.IsFixedLength();

	    modelBuilder.Entity<Event>()
		.Property(e => e.EventName)
		.IsUnicode(false);
	}
    }
}
