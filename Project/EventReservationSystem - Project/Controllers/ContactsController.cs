﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EventReservationSystem___Project;

namespace EventReservationSystem___Project.Controllers
{
    public class ContactsController : Controller
    {
        private ReservationModel db = new ReservationModel();

        // GET: Contacts
        public ActionResult Index()
        {
            return View(db.Contacts.ToList());
        }

        // GET: Contacts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // GET: Contacts/Create
        public ActionResult Create()
        {
	    Contact contact = new Contact();

	    var countries = GetAllCountries();
	    contact.countries = GetSelectListItems(countries);

	    return View(contact);
        }

        // POST: Contacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FirstName,LastName,Address,City,Province,Country,PostalCode,PhoneNumber,EmailAddress")] Contact contact)
        {
            if (ModelState.IsValid)
            {
		var countries1 = GetAllCountries();
		contact.countries = GetSelectListItems(countries1);
		db.Contacts.Add(contact);
		Manager.c = contact;
                db.SaveChanges();
                return RedirectToAction("Create","Events");
            }

	    var countries = GetAllCountries();

	    contact.countries = GetSelectListItems(countries);

	    return View(contact);
        }

        // GET: Contacts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
	    if (contact == null)
            {
                return HttpNotFound();
            }

	    var countries = GetAllCountries();
	    contact.countries = GetSelectListItems(countries);

	    return View(contact);
        }

        // POST: Contacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FirstName,LastName,Address,City,Province,Country,PostalCode,PhoneNumber,EmailAddress")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

	    var countries = GetAllCountries();
	    contact.countries = GetSelectListItems(countries);
	    return View(contact);
        }

        // GET: Contacts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: Contacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contact contact = db.Contacts.Find(id);
            db.Contacts.Remove(contact);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

	private IEnumerable<string> GetAllCountries()
	{
	    return new List<string>
	    {
		"Canada",
		"USA"
	    };
	}

	// This is one of the most important parts in the whole example.
	// This function takes a list of strings and returns a list of SelectListItem objects.
	// These objects are going to be used later in the SignUp.html template to render the
	// DropDownList.
	private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<string> elements)
	{
	    // Create an empty list to hold result of the operation
	    var selectList = new List<SelectListItem>();

	    // For each string in the 'elements' variable, create a new SelectListItem object
	    // that has both its Value and Text properties set to a particular value.
	    // This will result in MVC rendering each item as:
	    //     <option value="State Name">State Name</option>
	    foreach (var element in elements)
	    {
		selectList.Add(new SelectListItem
		{
		    Value = element,
		    Text = element
		});
	    }

	    return selectList;
	}

	public void countryChanged()
	{

	}

    }
}
