USE [CustomerPortal]
GO

/****** Object:  Table [dbo].[CreditCard]    Script Date: 24-Feb-18 6:40:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CreditCard](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreditCardType] [varchar](20) NOT NULL,
	[CreditCardNumber] [varchar](50) NOT NULL,
	[NameOnCard] [varchar](50) NOT NULL,
	[ExpirationDate] [date] NOT NULL,
	[CVV] [nchar](10) NOT NULL,
	[ContactID] [int] NOT NULL,
 CONSTRAINT [PK_CreditCard] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CreditCard]  WITH CHECK ADD  CONSTRAINT [FK_CreditCard_Contact] FOREIGN KEY([ContactID])
REFERENCES [dbo].[Contact] ([ID])
GO

ALTER TABLE [dbo].[CreditCard] CHECK CONSTRAINT [FK_CreditCard_Contact]
GO


