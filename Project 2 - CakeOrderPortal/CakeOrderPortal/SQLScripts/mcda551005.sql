-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: mcda551005
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cakeorderdetails`
--

DROP TABLE IF EXISTS `cakeorderdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cakeorderdetails` (
  `OrderId` int(11) NOT NULL AUTO_INCREMENT,
  `CakeName` varchar(256) DEFAULT NULL,
  `CakeType` varchar(256) DEFAULT NULL,
  `Weight` varchar(50) DEFAULT NULL,
  `DeliveryDate` datetime DEFAULT NULL,
  `DeliveryTime` varchar(16) DEFAULT NULL,
  `FirstName` varchar(256) DEFAULT NULL,
  `LastName` varchar(256) DEFAULT NULL,
  `StreetNumber` varchar(50) DEFAULT NULL,
  `Address` varchar(256) DEFAULT NULL,
  `City` varchar(256) DEFAULT NULL,
  `Province` varchar(256) DEFAULT NULL,
  `Country` varchar(256) DEFAULT NULL,
  `PostalCode` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`OrderId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cakeorderdetails`
--

LOCK TABLES `cakeorderdetails` WRITE;
/*!40000 ALTER TABLE `cakeorderdetails` DISABLE KEYS */;
INSERT INTO `cakeorderdetails` VALUES (1,'Red Velvet Fresh Cream Cake','EGG','1 KG','2018-03-28 00:00:00','09:00:00.0000000','Dinesh','Kumar','2155','Monastery Lane','Halifax','Nova Scotia','Canada','B3L 4P9'),(2,'Rich Fruit Cake','EGGLESS','2 KG','2018-03-27 00:00:00','10:00:00.0000000','Meghana','Chillal','1030','Somerset Apartments','Halifax','Nova Scotia','Canada','B3L 4P9');
/*!40000 ALTER TABLE `cakeorderdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (1,'guest','guest'),(2,'1234','1234');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentinformation`
--

DROP TABLE IF EXISTS `paymentinformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentinformation` (
  `EmailAddress` varchar(256) DEFAULT NULL,
  `PhoneNumber` varchar(256) DEFAULT NULL,
  `CreditCardType` varchar(256) DEFAULT NULL,
  `Name` varchar(256) DEFAULT NULL,
  `CreditCardNumber` varchar(256) DEFAULT NULL,
  `ExpirationDate` datetime DEFAULT NULL,
  `CodID` int(11) DEFAULT NULL,
  `PID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`PID`),
  KEY `CodID_idx` (`CodID`),
  CONSTRAINT `OrderID` FOREIGN KEY (`CodID`) REFERENCES `cakeorderdetails` (`OrderId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentinformation`
--

LOCK TABLES `paymentinformation` WRITE;
/*!40000 ALTER TABLE `paymentinformation` DISABLE KEYS */;
INSERT INTO `paymentinformation` VALUES ('meghana.chillal@gmail.com','9024127196','Master Card','Meghana Chillal','5332607335371084','2018-12-01 00:00:00',2,1),('dkdinesh7171@gmail.com','9024127247','Visa','Dinesh Kumar','4024007135751047','2029-12-01 00:00:00',1,2);
/*!40000 ALTER TABLE `paymentinformation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-28 22:32:24
