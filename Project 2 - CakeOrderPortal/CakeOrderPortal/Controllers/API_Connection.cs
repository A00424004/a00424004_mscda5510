﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace CakeOrderPortal
{
    public class API_Connection
    {
	private static readonly HttpClient client = new HttpClient();
	private static readonly string clientUrl = "http://localhost:8080/Cakekart/rest/Order/";

	public async Task<string> GetAsync(string path)
	{
	    var responseString = await client.GetStringAsync(clientUrl+path);
	    return responseString;
	}

	public async Task<string> PostAsync(string path, Dictionary<string,string> values)
	{
            //   var values = new Dictionary<string, string>
            //   {
            //{ "thing1", "hello" },
            //{ "thing2", "world" }
            //   };
            var json = new JavaScriptSerializer().Serialize(values.ToDictionary(item => item.Key.ToString(), item => item.Value.ToString()));

            //var content = new FormUrlEncodedContent(json);
	    var response = await client.PostAsync(clientUrl+path, new StringContent(
        json.ToString(),
        Encoding.UTF8,
        "application/json"));
	    var responseString = await response.Content.ReadAsStringAsync();
	    return responseString;
	}
    }
}