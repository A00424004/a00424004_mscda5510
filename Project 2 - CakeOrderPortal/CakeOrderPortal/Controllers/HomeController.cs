﻿using CakeOrderPortal.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;



namespace CakeOrderPortal.Controllers
{
    public class HomeController : Controller
    {
        private API_Connection aPI_Connection;
        int CodId;
        PaymentInformation _paymentInformation;
        CakeOrderDetail _cakeOrderInformation;

        private CakeDeliveryModel db = new CakeDeliveryModel();

        public ActionResult Index()
        {
            if (aPI_Connection == null)
                aPI_Connection = new API_Connection();
            return View();
        }



        //GET:Home/Order_Cake
        public ActionResult Order_Cake()
        {
            ViewBag.Message = "Cake Order Page.";
            CakeOrderDetail cakeOrderInformation = new CakeOrderDetail();

            //Get All Countries
            var countries = GetCountries();
            cakeOrderInformation.countries = GetSelectListItems(countries);
            return View(cakeOrderInformation);
        }


        // POST: CakeOrderInformations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Order_Cake([Bind(Include = "LastName,CakeName,CakeType,Weight,DeliveryDate,DeliveryTime,FirstName,StreetNumber,Address,City,Province,Country,PostalCode")] CakeOrderDetail cakeOrderInformation)
        {

            if (ModelState.IsValid)
            {
                db.CakeOrderDetails.Add(cakeOrderInformation);
                MultipleView multipleView = new MultipleView();
                //db.SaveChanges();
                //ViewBag.OrderId = cakeOrderInformation.OrderId;
                return RedirectToAction("Payment", cakeOrderInformation);

            }
            var countries = GetCountries();
            cakeOrderInformation.countries = GetSelectListItems(countries);

            return View(cakeOrderInformation);

        }

        //GET:Home/Payment
        public ActionResult Payment(CakeOrderDetail cakeOrderInformation)
        {
            ViewBag.Message = "Payment Page.";
            MultipleView multipleView = new MultipleView();
            multipleView.Address = cakeOrderInformation.Address;
            multipleView.CakeName = cakeOrderInformation.CakeName;
            multipleView.CakeType = cakeOrderInformation.CakeType;
            multipleView.City = cakeOrderInformation.City;
            multipleView.Country = cakeOrderInformation.Country;
            multipleView.DeliveryDate = cakeOrderInformation.DeliveryDate;
            multipleView.DeliveryTime = cakeOrderInformation.DeliveryTime;
            multipleView.FirstName = cakeOrderInformation.FirstName;
            multipleView.LastName = cakeOrderInformation.LastName;
            multipleView.PostalCode = cakeOrderInformation.PostalCode;
            multipleView.Province = cakeOrderInformation.Province;
            multipleView.StreetNumber = cakeOrderInformation.StreetNumber;
            multipleView.Weight = cakeOrderInformation.Weight;
            return View(multipleView);
        }


        // POST: PaymentInformations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Payment([Bind(Include = "EmailAddress,CreditCardType,PhoneNumber,Name,CreditCardNumber,ExpirationDate")] PaymentInformation paymentInformation, CakeOrderDetail cakeOrderInformation)
        {
            if (ModelState.IsValid)
            {
                //db.PaymentInformations.Add(paymentInformation);
                //db.CakeOrderDetails.Add(cakeOrderInformation);
                //db.SaveChanges();

                _cakeOrderInformation = cakeOrderInformation;
                Task orderTask = new Task(PostOrderDetails);
                orderTask.Start();

                CodId = cakeOrderInformation.OrderId;
                CodId = 1;
                _paymentInformation = paymentInformation;
                Task paymentTask = new Task(PostPaymentInformation);
                paymentTask.Start();
            }
            return RedirectToAction("Success", paymentInformation);
        }

        async void PostOrderDetails()
        {
            //{
            //    "cakeName": "Red Velvet Fresh Cream Cake",
            // "cakeType": "EGGLESS",
            // "weight": "2 KG",
            // "deliveryDate": "2019-03-28T03:00:00Z[UTC]",
            // "deliveryTime": "09:00:00.0000000",
            // "firstName": "Ranjit",
            // "lastName": "Kumar",
            // "city": "Halifax",
            // "province": "Nova Scotia",
            // "country": "Canada",
            // "postalCode": "B3H 3B5",
            // "streetNumber": "666",
            // "address": "Monastery Lane"
            //}

            Dictionary<string, string> postData = new Dictionary<string, string> { };

            postData.Add("cakeName", _cakeOrderInformation.CakeName);
            postData.Add("cakeType", _cakeOrderInformation.CakeType);
            postData.Add("weight", _cakeOrderInformation.Weight);

            string date = _cakeOrderInformation.DeliveryDate.Year + "-" + _cakeOrderInformation.DeliveryDate.Month + "-" + _cakeOrderInformation.DeliveryDate.Date + "T03:00:00Z[UTC]";
            postData.Add("deliveryDate", date);

            string time = _cakeOrderInformation.DeliveryTime.Hours + ":" + _cakeOrderInformation.DeliveryTime.Minutes + ":00.0000000";
            postData.Add("deliveryTime", time);

            postData.Add("firstName", _cakeOrderInformation.FirstName);
            postData.Add("lastName", _cakeOrderInformation.LastName);
            postData.Add("city", _cakeOrderInformation.City);
            postData.Add("province", _cakeOrderInformation.Province);
            postData.Add("country", _cakeOrderInformation.Country);
            postData.Add("postalCode", _cakeOrderInformation.PostalCode);
            postData.Add("streetNumber", _cakeOrderInformation.StreetNumber);
            postData.Add("address", _cakeOrderInformation.Address);

            if (aPI_Connection == null)
                aPI_Connection = new API_Connection();
            try
            {
                var response = await aPI_Connection.PostAsync("postorderdetails", postData);
            }
            catch (Exception err)
            {

            }

            int a = 10;
        }

        async void PostPaymentInformation()
        {
            //{
            //    "EmailAddress": "ranjit@smu.ca",
            // "PhoneNumber": "9022208565",
            // "CreditCardType" : "VISA",
            // "Name":"Ranjit Mishra",
            // "CreditCardNumber": "4536272656543425",
            // "ExpirationDate" :"2021-04-25 00:00:00",
            // "CodId": "1"
            //}

            Dictionary<string, string> postData = new Dictionary<string, string> { };

            postData.Add("EmailAddress", _paymentInformation.EmailAddress);
            postData.Add("PhoneNumber", _paymentInformation.PhoneNumber);
            postData.Add("CreditCardType", _paymentInformation.CreditCardType);
            postData.Add("Name", _paymentInformation.Name);
            postData.Add("CreditCardNumber", _paymentInformation.CreditCardNumber);

            string expiry = _paymentInformation.ExpirationDate.Value.Year + "-" + _paymentInformation.ExpirationDate.Value.Month + "-01 00:00:00";
            postData.Add("ExpirationDate", expiry);
            postData.Add("CodId", CodId.ToString());

            if (aPI_Connection == null)
                aPI_Connection = new API_Connection();
            try
            {
                var response = await aPI_Connection.PostAsync("postpaymentinfo", postData);
            }
            catch (Exception err)
            {

            }
            int b = 10;
        }

        // GET: CakeDeliveryDetails/Edit/5
        public ActionResult Edit(MultipleView multipleView)
        {
            if (multipleView == null)
            {
                return HttpNotFound();
            }
            CakeOrderDetail cakeOrderInformation = new CakeOrderDetail();
            cakeOrderInformation.Address = multipleView.Address;
            cakeOrderInformation.CakeName = multipleView.CakeName;
            cakeOrderInformation.CakeType = multipleView.CakeType;
            cakeOrderInformation.City = multipleView.City;
            cakeOrderInformation.Country = multipleView.Country;
            cakeOrderInformation.DeliveryDate = multipleView.DeliveryDate;
            cakeOrderInformation.DeliveryTime = multipleView.DeliveryTime;
            cakeOrderInformation.FirstName = multipleView.FirstName;
            cakeOrderInformation.LastName = multipleView.LastName;
            cakeOrderInformation.PostalCode = multipleView.PostalCode;
            cakeOrderInformation.Province = multipleView.Province;
            cakeOrderInformation.StreetNumber = multipleView.StreetNumber;
            cakeOrderInformation.Weight = multipleView.Weight;
            var countries = GetCountries();
            cakeOrderInformation.countries = GetSelectListItems(countries);
            return View(cakeOrderInformation);
        }

        // POST: CakeDeliveryDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LastName,CakeName,CakeType,Weight,DeliveryDate,DeliveryTime,FirstName,StreetNumber,Address,City,Province,Country,PostalCode")] CakeOrderDetail cakeOrderInformation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cakeOrderInformation).State = EntityState.Modified;
                return RedirectToAction("Payment", cakeOrderInformation);
            }
            var countries = GetCountries();
            cakeOrderInformation.countries = GetSelectListItems(countries);
            return View(cakeOrderInformation);
        }

        public ActionResult Success(PaymentInformation paymentInformation)
        {
            if (paymentInformation.Name == null)
            {
                return HttpNotFound();
            }

            return View(paymentInformation);
        }

        // GET: CakeOrderDetails/Details/5
        public ActionResult Details(MultipleView multipleView)
        {
            if (multipleView == null)
            {
                return HttpNotFound();
            }
            return View(multipleView);
        }


        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        private IEnumerable<string> GetCountries()
        {
            return new List<string>
            {
                "Canada",
                "US"
            }
            ;
        }

        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<string> elements)
        {
            var selectList = new List<SelectListItem>();
            foreach (var element in elements)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element,
                    Text = element
                });
            }
            return selectList;
        }

    }
}




